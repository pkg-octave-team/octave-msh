Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: MSH package for Octave
Upstream-Contact: The Octave Community <octave-maintainers@octave.org>
Source: https://gnu-octave.github.io/packages/msh/

Files: *
Copyright: 2006-2010, 2012-2013 Carlo de Falco
           2006-2010, 2012 Massimiliano Culpo
           2013-2014 Marco Vassalo
License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 2, can be found in the file
 `/usr/share/common-licenses/GPL-2'.

Files: debian/*
Copyright: 2008 Olafur Jens Sigurdsson <ojsbug@gmail.com>
           2008, 2009, 2012, 2013, 2017-2019, 2022 Rafael Laboissière <rafael@debian.org>
           2009-2011 Thomas Weber <tweber@debian.org>
           2011, 2013-2014 Sébastien Villemot <sebastien@debian.org>
License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 3, can be found in the file
 `/usr/share/common-licenses/GPL-3'.
